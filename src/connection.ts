import * as dotenv from 'dotenv';
import mongoose = require('mongoose');

dotenv.config();

const { NODE_ENV, MONGO_URL = '' } = process.env;

mongoose.set('useFindAndModify', false);

mongoose.Promise = global.Promise;

mongoose.connection
  .on('connected', () => {
    if (NODE_ENV !== 'test') {
      console.log(`Connected to the database: ${MONGO_URL}`);
    }
  })
  .on('disconnected', () => {
    console.log(`Disconnected from the database: ${MONGO_URL}`);
  })
  .on('error', error => {
    console.log(`Database connection error: ${MONGO_URL}`, error);
  });

export function connect() {
  return mongoose.connect(
    MONGO_URL,
    { useNewUrlParser: true },
  );
}

export function disconnect() {
  return mongoose.connection.close();
}